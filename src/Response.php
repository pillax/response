<?php
namespace vendor\pillax\response\src;

class Response {

    /** possible response codes */
    const RESPONSE_CODE_200 = 200;
    const RESPONSE_CODE_404 = 404;

    /** @var int response code */
    private static $responseCode = 200;

    /**
     * Json response
     * @param array $data
     */
    public static function json(array $data) : void {
        http_response_code(self::$responseCode);
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    /**
     * HTML response
     * @param string $data
     */
    public static function html(string $data) : void  {
        http_response_code(self::$responseCode);
        header('Content-Type:text/html');
        echo $data;
    }

    /**
     * Text response
     * @param array $data
     */
    public static function text(array $data) : void  {
        http_response_code(self::$responseCode);
        header("Content-Type: text/plain");
        echo $data;
    }

    /**
     * Set response code
     * @param int $responseCode
     */
    public static function setResponseCode(int $responseCode) : void  {
        self::$responseCode = (int) $responseCode;
    }


}